import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserAuthComponent} from './auth/user-auth/user-auth.component';
import {HomeComponent} from './solveit-website/home/home.component';
import {ConsoleIndexComponent} from './solveit-console/console-index/console-index.component';
import {DashboardComponent} from './solveit-console/dashboard/dashboard.component';
import {ProblemsComponent} from './solveit-console/problems/problems.component';
import {SessionsComponent} from './solveit-console/sessions/sessions.component';
import {SingupConsultantComponent} from './solveit-console/singup-consultant/singup-consultant.component';
import {PostPropblemComponent} from './solveit-console/problems/post-propblem/post-propblem.component';
import {AccountSettingsComponent} from './solveit-console/account-settings/account-settings.component';
import {LiveCallComponent} from './solveit-console/live-call/live-call.component';
import {ViewProblemComponent} from './solveit-console/problems/view-problem/view-problem.component';
import {JwtGuardGuard} from './auth-guard/jwt-guard.guard';
import {ReviewsComponent} from './solveit-console/reviews/reviews.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'auth', component: UserAuthComponent},
  {
    path: 'console', canActivate: [JwtGuardGuard], component: ConsoleIndexComponent,
    children: [
      {path: 'dashboard', canActivateChild: [JwtGuardGuard], component: DashboardComponent},
      {
        path: 'problems',
        children: [
          {path: '', canActivateChild: [JwtGuardGuard], component: ProblemsComponent},
          {path: 'post', canActivateChild: [JwtGuardGuard], component: PostPropblemComponent},
          {path: 'post/:id', canActivateChild: [JwtGuardGuard], component: PostPropblemComponent},
          {path: 'view/post/:id', canActivateChild: [JwtGuardGuard], component: ViewProblemComponent},
        ]
      },
      {
        path: 'sessions',
        children: [
          {path: '', canActivateChild: [JwtGuardGuard], component: SessionsComponent},
          {path: 'live-call', canActivateChild: [JwtGuardGuard], component: LiveCallComponent}
        ],
      },
      {
        path: 'settings', canActivateChild: [JwtGuardGuard] , component: AccountSettingsComponent,
      },
      {path: 'reviews', canActivateChild: [JwtGuardGuard], component: ReviewsComponent},
      {
        path: 'signup-consultant', canActivateChild: [JwtGuardGuard], component: SingupConsultantComponent,
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
