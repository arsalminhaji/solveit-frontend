import { Injectable } from '@angular/core';
import {UserType} from './common/enums';
import {BehaviorSubject, Observable} from 'rxjs';
import * as uuid from 'uuid';
import {Router} from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private globalCurrentUserType = new  BehaviorSubject(null);
  globalCurrentUserTypeLocal  = this.globalCurrentUserType.asObservable();
  private currentUserSubject: BehaviorSubject<string>;
  public currentUser: Observable<string>;

  constructor(private readonly router: Router) {
    this.currentUserSubject = new BehaviorSubject<string>(localStorage.getItem('userJwtToken'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  setUserType(isConsultant) {
  if (isConsultant) {
    localStorage.setItem('userType', JSON.stringify([UserType.PROBLEM_SOLVER, UserType.USER]) );

  } else {
    localStorage.setItem('userType', JSON.stringify([UserType.USER]) );
  }
  localStorage.setItem('currentUserType', UserType.USER.toString());
  return this.globalCurrentUserType.next(UserType.USER);
  }

  returnUserType() {
    return JSON.parse(localStorage.getItem('userType'));
  }

  getCurrentUser() {
    return this.currentUserSubject.value;
  }

  switchUserType(typeEnum) {
    const localStorageUserType = JSON.parse(localStorage.getItem('userType'));
    if (localStorageUserType.includes(typeEnum)) {
      localStorage.setItem('currentUserType', typeEnum );
      return this.globalCurrentUserType.next(typeEnum);
    } else {
      localStorage.setItem('currentUserType', UserType.USER.toString());
      return  this.globalCurrentUserType.next(UserType.USER.toString());
    }
  }

  returnCurrentUserType() {
      return localStorage.getItem('currentUserType');
  }

  setJWTToken(jwtToken) {
    this.currentUserSubject.next(jwtToken);
    localStorage.setItem('userJwtToken', jwtToken);
  }

  getJWTToken() {
     return localStorage.getItem('userJwtToken');
  }


  saveFCMToken(fcmToken) {
    localStorage.setItem('userFcmToken', fcmToken);
  }

  getFCMToken() {
    return localStorage.getItem('userFcmToken');
  }

  saveUniqueBrowserUUID(browserUniqueId) {
    localStorage.setItem('unqiueBrowserId', browserUniqueId);
  }

  getUniqueBrowserUUID() {
    return localStorage.getItem('unqiueBrowserId');
  }

  logOut() {
    localStorage.removeItem('userJwtToken');
    localStorage.removeItem('userFcmToken');
    localStorage.removeItem('currentUserType');
    this.currentUserSubject.next(null);
    this.router.navigate(['auth']);
  }

  updateOrAddUniqueDeviceToServer() {
    if (this.getUniqueBrowserUUID() == undefined) {
      this.saveUniqueBrowserUUID(uuid.v4());
      return this.getUniqueBrowserUUID();
    } else {
      return this.getUniqueBrowserUUID();
    }
  }


}
