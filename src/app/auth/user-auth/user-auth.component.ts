import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthToggle} from '../../common/enums';
import {CustomAuthService} from '../auth.service';
import {Router} from '@angular/router';
import {concatMap} from 'rxjs/operators';
import {EMPTY, from, of} from 'rxjs';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';
import {SocialLoginService} from '../../common/social-login.service';

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.scss']
})
export class UserAuthComponent implements OnInit {

  public toggleState = AuthToggle;
  public toggleValue = this.toggleState.SIGNUP;



  private signupFormGroup = new FormGroup({
    email: new FormControl('',[
      Validators.required,
      Validators.pattern('\\s*[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\\s*')
    ]),
    username: new FormControl('',[
      Validators.required,
    ]),
    phone_number : new FormControl('',[
      Validators.required,
    ]),
    cpassword: new FormControl('',[
      Validators.required,
    ]),
    password: new FormControl('',[
      Validators.required,
    ])
  });
  private loginFormGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('\\s*[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\\s*')
    ]),
    password: new FormControl('', [
      Validators.required,
    ])
  });

  get email() {
    return this.loginFormGroup.get('email');
  }

  get password() {
    return this.loginFormGroup.get('password');
  }

  constructor(private customAuthService: CustomAuthService , private router: Router, private socialAuthService: AuthService , private socialLoginService: SocialLoginService) { }

  ngOnInit() {
  }

  googleLogin() {
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        // Now sign-in with userData
        console.log(userData)
        this.socialLoginService.googleLogin(userData).subscribe((successResponseFromGoogle) =>{
          console.log(successResponseFromGoogle);
        }, error => {

        });
      }
    );
  }

  facebookLogin() {
   this.socialLoginService.facebookLogin();
  }

  // getUserInfoFromFacebookLogin() {
  //   return from(new Promise<any>(resolve => FB.api('/me', 'GET', {fields: 'id,name,email'})))
  //     .pipe(concatMap(({ userInfoDetail }) => {
  //       console.log(userInfoDetail, 'userDetail from facebook')
  //       if (!userInfoDetail) { return EMPTY; }
  //       return of(userInfoDetail);
  //     }));
  // }


  switchForms(toggleValue) {
    if (toggleValue === this.toggleState.SIGNUP) {
      this.toggleValue = toggleValue ;
    } else if (toggleValue === this.toggleState.LOGIN) {
      this.toggleValue = this.toggleState.LOGIN ;
    } else {
      this.toggleValue = this.toggleState.LOGIN;
    }
  }

  onSubmitLogin() {
  const loginFormData = this.loginFormGroup.value;
  if (this.loginFormGroup.valid) {
    this.customAuthService.loginUser(loginFormData).subscribe((loginSuccessResponse) => {
      if (loginSuccessResponse) {
        this.router.navigate(['/console/dashboard']);
      }
      this.loginFormGroup.reset();
    }, (error) => {
      console.error(error);
      this.loginFormGroup.reset();
    });
  } else {
    this.loginFormGroup.reset();
  }

  }
  onSubmitSignUp() {
    const signUpFormData = this.signupFormGroup.value;
    if (this.signupFormGroup.valid) {
      this.customAuthService.signUpUser(signUpFormData).subscribe((signUpSuccessResponse) => {
        this.toggleValue = this.toggleState.LOGIN;
        this.signupFormGroup.reset();
      }, (error) => {
        console.error(error);
        this.signupFormGroup.reset();
      });
    } else {
      this.signupFormGroup.reset();
    }

  }
}
