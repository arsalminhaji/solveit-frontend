import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, concatMap, map, tap} from 'rxjs/operators';
import {Global} from '../global';
import {CommonService} from '../common.service';
import {ResponseInterface} from '../interface/sucess.response.interface';
import {FcmMessagingService} from '../common/fcm-messaging.service';
import {EMPTY, from, of} from 'rxjs';
declare const FB: any;

@Injectable({
  providedIn: 'root'
})
export class CustomAuthService {
  private APIurl;

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService, private fcmMessagingService: FcmMessagingService) {
    this.APIurl = global.url;
  }

  loginUser(loginFormData) {
    const browserFingerPrint = this.commonService.updateOrAddUniqueDeviceToServer();
    loginFormData.deviceType = 'BROWSER';
    loginFormData.deviceUniqueToken = browserFingerPrint;
    return  this.httpClient.post(this.APIurl + 'auth/login', loginFormData).pipe(map ((response: ResponseInterface) => {
      const userJson = response.data;
      this.commonService.setUserType(userJson.is_consultant);
      this.commonService.setJWTToken(userJson.token);
      return userJson;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
      throw err;
      })
    );
  }

  signUpUser(signupFormData) {
    return  this.httpClient.post(this.APIurl + 'auth/register', signupFormData).pipe(map ((response: ResponseInterface) => {
        return response.message;
    }),
    tap((mappedData) => {console.log(mappedData); }),
     catchError(err => {
        throw err;
    }));
  }



  forgotPasswordEmailSend() {

  }

  forgotPassword() {

  }

}
