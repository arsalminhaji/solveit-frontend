import { TestBed } from '@angular/core/testing';

import { ConsoleIndexService } from './console-index.service';

describe('ConsoleIndexService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsoleIndexService = TestBed.get(ConsoleIndexService);
    expect(service).toBeTruthy();
  });
});
