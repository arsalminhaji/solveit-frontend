import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {CommonService} from '../../common.service';
import {FcmMessagingService} from '../../common/fcm-messaging.service';
import {BehaviorSubject} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {ResponseInterface} from '../../interface/sucess.response.interface';
import {PushNotificationBuilder} from '../../common/PushNotificationBuilder';

@Injectable({
  providedIn: 'root'
})
export class ConsoleIndexService {
  private currentUserType: null;
  private APIurl: string;
  private globalUserType: string;
  message = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService , private fcmMessagingService: FcmMessagingService) {
    this.APIurl = global.url;
    this.commonService.globalCurrentUserTypeLocal.subscribe((usertype) => this.currentUserType = usertype);
    this.globalUserType = this.commonService.returnUserType();
    this.fcmMessagingService.currentMessage.subscribe((newMessage) => {
      this.message.next(newMessage);
    });
    this.commonService.updateOrAddUniqueDeviceToServer();
  }

  returnGlobalUserType() {
    return this.globalUserType;
  }

  requestUserPermissionForNotification() {
    this.fcmMessagingService.requestPermission();
  }
  recieveNotifications() {
    this.fcmMessagingService.receiveMessage();
  }

  userInfo() {
    return this.httpClient.get(this.APIurl + 'users/user-profile').pipe(map((response: ResponseInterface) => {
        return response.data;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }

  onLogout() {
    this.commonService.logOut();
  }

  getNotificationFromServer() {
    return this.httpClient.get(this.APIurl + 'notifications').pipe(map((response: ResponseInterface) => {
        return response.data.map((notificationData) => {
          const notificationBody = notificationData.notificationBody.notification;
          return new PushNotificationBuilder(notificationBody.body, notificationBody.icon
            , notificationBody.click_action , notificationBody.title);
        });
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }


}
