import { Component, OnInit } from '@angular/core';
import {ConsoleIndexService} from './console-index.service';
import {Observable} from 'rxjs';
import {not} from 'rxjs/internal-compatibility';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-console-index',
  templateUrl: './console-index.component.html',
  styleUrls: ['./console-index.component.scss']
})
export class ConsoleIndexComponent implements OnInit {
  consoleIndexComponentUserType;
  show;
  notificationArray: any = [];
  profileInfo$: Observable<any>;


  constructor(private consoleIndexService: ConsoleIndexService) { }

  ngOnInit() {
    this.consoleIndexComponentUserType = this.consoleIndexService.returnGlobalUserType();
    this.show = this.consoleIndexService.message;
    this.show.subscribe((UpdatedValue) =>{
      console.log(UpdatedValue);
      this.notificationArray.push(UpdatedValue);
    });
    this.consoleIndexService.requestUserPermissionForNotification();
    this.consoleIndexService.recieveNotifications();
    this.preLoadedNotifications();
    this.UserInfo();
  }

  logout() {
    this.consoleIndexService.onLogout();
  }

  UserInfo() {
  this.profileInfo$ = this.consoleIndexService.userInfo();
  }

  preLoadedNotifications() {
    this.consoleIndexService.getNotificationFromServer().subscribe((notifications) => {
    this.notificationArray = notifications;
    console.log(this.notificationArray);
    }, error => {
      console.log(error);
    });
  }

}
