
export class Sessions {
  public id: string;
  public problemTitle: string;
  public problemDescription: string;
  public startTime: string;
  public sessionProblemId: string;
  public sessionToken: string;
  public sessionId: string;
  constructor(id, problemTitle, problemDescription, startTime , sessionProblemId, sessionToken , sessionId) {
    this.id = id;
    this.problemTitle = problemTitle;
    this.problemDescription = problemDescription;
    this.startTime = this.convertStartTimeDate(startTime);
    this.sessionProblemId = sessionProblemId;
    this.sessionToken = sessionToken;
    this.sessionId = sessionId;
  }

  convertStartTimeDate(startTime) {
    return new Date(startTime).toLocaleDateString(document.documentElement.lang, {
      day: 'numeric',
      month: 'short',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit'
    });
  }
}
