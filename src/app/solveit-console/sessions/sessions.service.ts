import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {CommonService} from '../../common.service';
import {catchError, filter, map, shareReplay, tap} from 'rxjs/operators';
import {ResponseInterface} from '../../interface/sucess.response.interface';
import {Sessions} from './sessions';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  private APIurl;
  private currentUserType;
  detectChangeSubject = new  BehaviorSubject(null);

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService) {
    this.APIurl = global.url;
    this.commonService.globalCurrentUserTypeLocal.subscribe((usertype) => {
      if (usertype == null) {
        // tslint:disable-next-line:radix
        this.currentUserType = parseInt(this.commonService.returnCurrentUserType());
      } else {
        this.currentUserType = usertype;
        this.detectChangeSubject.next(1);
      }
    });
  }
  getUpcomingSessionAsUserOrProblemSolver() {
    if (!this.currentUserType) {
      return this.httpClient.get(this.APIurl + 'call-sessions').pipe(map((response: ResponseInterface) => {
        return  response.data.filteredUserSession.map((filterUserSession) => {
          console.log(filterUserSession);
          return  new Sessions(filterUserSession._id, filterUserSession.session_problem_id.title,
            filterUserSession.session_problem_id.description, filterUserSession.start_time ,
            filterUserSession.session_problem_id._id , filterUserSession.session_token, filterUserSession._id);
        });
      }),
        tap((mappedData) => {console.log(mappedData); }),
        catchError(err => {
          console.log(err);
          throw err;
        }), shareReplay());
    } else {
      return this.httpClient.get(this.APIurl + 'call-sessions/consultant').pipe(map((response: ResponseInterface) => {
        return  response.data.filteredConsultantProblems.map((filterConsultantSession) => {
            console.log(filterConsultantSession);
            return new Sessions(filterConsultantSession._id, filterConsultantSession.session_problem_id.title,
               filterConsultantSession.session_problem_id.description, filterConsultantSession.start_time,
              filterConsultantSession.session_problem_id._id , filterConsultantSession.session_token, filterConsultantSession._id);
          });
        }),
        tap((mappedData) => {console.log(mappedData); }),
        catchError(err => {
          throw err;
        }), shareReplay());
    }

  }
  getSessionHistoryAsUserOrProblemSolver() {

  }


}
