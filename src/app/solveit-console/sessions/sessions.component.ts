import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProblemsService} from '../problems/problems.service';
import {SessionsService} from './sessions.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit, OnDestroy {

  constructor(private sessionService: SessionsService) {

  }
  SessionSubject: any;
  upcomingSessions$: Observable<any>;
  sessionHistory$: Observable<any>;
  ngOnInit() {
    this.SessionSubject = this.sessionService.detectChangeSubject.subscribe((value) => {
      this.loadSessionOnUI();
    });
  }

  loadSessionOnUI() {
    this.upcomingSessions$ = this.sessionService.getUpcomingSessionAsUserOrProblemSolver();
  }

  loadSessionHistoryOnUI() {
    this.sessionService.getSessionHistoryAsUserOrProblemSolver();
  }

  ngOnDestroy(): void {
    this.SessionSubject.unsubscribe();
  }


}
