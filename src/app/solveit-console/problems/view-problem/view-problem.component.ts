import { Component, OnInit } from '@angular/core';
import {ProblemsService} from '../problems.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-view-problem',
  templateUrl: './view-problem.component.html',
  styleUrls: ['./view-problem.component.scss']
})
export class ViewProblemComponent implements OnInit {

  constructor(private readonly problemsService: ProblemsService, private activateRoute: ActivatedRoute) { }

  ngOnInit() {
   // this.getSingleProblemData();
  }
  getSingleProblemData(problemId) {
    this.problemsService.getSingleProblem(problemId);
  }
}
