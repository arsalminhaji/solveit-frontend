import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProblemsService} from './problems.service';
import {Observable} from 'rxjs';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ProblemModalViewComponent} from './problem-modal-view/problem-modal-view.component';
import {problemChangeEnum} from '../../common/enums';

@Component({
  selector: 'app-problems',
  templateUrl: './problems.component.html',
  styleUrls: ['./problems.component.scss']
})
export class ProblemsComponent implements OnInit , OnDestroy {

  problemViewModalRef: BsModalRef;
  problemLocalSubject: any;
  problemList$: Observable<any> = new Observable<any>();
  tempEnum = problemChangeEnum;
  constructor(private modalService: BsModalService, private problemsService: ProblemsService) {

  }

  ngOnInit() {
    this.problemLocalSubject = this.problemsService.detectChangeSubject.subscribe((value) => {
      this.loadProblemsOnUI();
    });
  }

  loadProblemsOnUI() {
    this.problemList$ = this.problemsService.getProblemsAsUserOrProblemSolver();
  }

  ngOnDestroy(): void {
    this.problemLocalSubject.unsubscribe();
  }

  openViewProblemModal(problemId) {
    const initialState = {
    problemId
    };
    this.problemViewModalRef = this.modalService.show(ProblemModalViewComponent, {initialState});
  }
  setUserTypeOnRefresh() {
  }

  viewOneProblemsOnUI() {

  }

}
