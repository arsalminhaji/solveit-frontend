export class Problem {
  public id: string;
  public title: string;
  public status: string;
  public description: string;
  public noOfViews: string;
  public noOfRequest: string;
  public viewStatus: string;
  constructor(id, title, status = 'RESOLVED', description , noOfViews, viewStatus) {
    this.id = id;
    this.title = title;
    this.status = status;
    this.description = description;
    this.noOfViews = noOfViews;
    this.viewStatus = viewStatus;

  }

}
