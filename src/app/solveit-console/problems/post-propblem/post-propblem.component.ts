import {Component, EventEmitter, OnInit} from '@angular/core';
import {ProblemsService} from '../problems.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {problemChangeEnum} from '../../../common/enums';
import {UploaderOptions, UploadFile, UploadInput, UploadOutput} from 'ngx-uploader';

@Component({
  selector: 'app-post-propblem',
  templateUrl: './post-propblem.component.html',
  styleUrls: ['./post-propblem.component.scss']
})
export class PostPropblemComponent implements OnInit {
  problemId;
  quillConfiguration = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      [{ color: [] }, { background: [] }],
      ['link'],
      ['clean'],
    ],
  };
  private postProblemForm = new FormGroup({
    title: new FormControl('', [
      Validators.required,
    ]),
    description: new FormControl('', [
    ])
  });
  problemStatus;
  problemAttachFiles: [];

  get title() {
    return this.postProblemForm.get('title');
  }

  get description() {
    return this.postProblemForm.get('description');
  }

  uploadInput: EventEmitter<UploadInput>;
  options: UploaderOptions;

  constructor(private readonly problemsService: ProblemsService, public route: ActivatedRoute) { }

  ngOnInit() {
    this.populateFormToEdit();
  }

  addOrUpdateProblem(isSubmitted = false) {
    if  (this.postProblemForm.valid) {
      const problemRequest = {
        title: this.title.value,
        description: this.description.value,
        is_submitted: isSubmitted,
        status: this.problemStatus || problemChangeEnum.DRAFT ,
        document_attachments_url: [],
      };
      this.problemsService.postAProblem(problemRequest, this.problemId).subscribe((problemSuccess) => {
        this.problemId = problemSuccess._id;
        this.description.setValue(problemSuccess.description);
        this.title.setValue(problemSuccess.title);
        this.problemStatus = problemSuccess.status;
        this.problemAttachFiles = problemSuccess.document_attachments_url;
      }, error => {
          console.log(error);
      });
    } else {
      console.log('form incorrect filled ');
    }
  }

  populateFormToEdit() {
    if (this.route.snapshot.paramMap.get('id')) {
      this.problemsService.getSingleProblem(this.route.snapshot.paramMap.get('id')).subscribe((singleProblem) => {
        console.log(singleProblem);
        this.problemId = singleProblem._id;
        this.description.setValue(singleProblem.description);
        this.title.setValue(singleProblem.title);
        this.problemStatus = singleProblem.status;
        this.problemAttachFiles = singleProblem.document_attachments_url;
      }, error => {
        console.log(error);
      });
    } else {
    console.log('do nothing');
    }
  }


  uploadImages(files) {
    console.log(files);
    this.problemsService.uploadProblemFiles(this.problemId, files).subscribe((uploadedSucceussFully)=>{
      console.log(uploadedSucceussFully);
    });
  }

  viewProblem() {

  }

}
