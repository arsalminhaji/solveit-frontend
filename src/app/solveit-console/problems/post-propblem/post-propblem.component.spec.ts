import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostPropblemComponent } from './post-propblem.component';

describe('PostPropblemComponent', () => {
  let component: PostPropblemComponent;
  let fixture: ComponentFixture<PostPropblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostPropblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostPropblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
