import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Global} from '../../global';
import {CommonService} from '../../common.service';
import {catchError, map, shareReplay, tap} from 'rxjs/operators';
import {ResponseInterface} from '../../interface/sucess.response.interface';
import {Problem} from './problem';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProblemsService {
  private APIurl;
  private currentUserType;
  detectChangeSubject = new  BehaviorSubject(null);
  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService) {
    this.APIurl = global.url;
    this.commonService.globalCurrentUserTypeLocal.subscribe((usertype) => {
      if (usertype == null) {
        // tslint:disable-next-line:radix
        this.currentUserType = parseInt(this.commonService.returnCurrentUserType());
      } else {
        this.currentUserType = usertype;
        this.detectChangeSubject.next(1);
      }
    });
  }

  getProblemsAsUserOrProblemSolver() {
    if (!this.currentUserType) {
      return this.httpClient.get(this.APIurl + 'problems').pipe(map((response: ResponseInterface) => {
        return  response.data.filteredUserProblems.map((filteredUserProblem) => {

          return new Problem(filteredUserProblem._id, filteredUserProblem.title, filteredUserProblem.status,
             filteredUserProblem.description, filteredUserProblem.views_count, filteredUserProblem.privacy_status);
        });
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
          throw err;
      }),
      shareReplay());
    } else {
      return this.httpClient.get(this.APIurl + 'problems/consultant').pipe(map((response: ResponseInterface) => {
        return  response.data.filteredConsultantProblems.map((filteredConsultantProblems) => {
return new Problem(filteredConsultantProblems._id, filteredConsultantProblems.title, filteredConsultantProblems.status,
   filteredConsultantProblems.description, filteredConsultantProblems.views_count, filteredConsultantProblems.privacy_status);
          });
        }),
        tap((mappedData) => {console.log(mappedData); }),
        catchError(err => {
          throw err;
        }),
        shareReplay());
    }
  }

  postAProblem(problemJsonData, problemId?: string) {
      let postProblem;
      const problemPutObject = {
        url: this.APIurl + 'problems',
        body: problemJsonData
      };
      if (problemId) {
        const params = new HttpParams()
          .set('problemId', problemId);
        postProblem =  this.httpClient.put(problemPutObject.url, problemPutObject.body , {params});
      } else {
         postProblem =  this.httpClient.put(problemPutObject.url, problemPutObject.body);
      }
      return postProblem.pipe(map((response: ResponseInterface) => {
        return response.data;
     }),
     tap((data) => {console.log(data); }),
     catchError(err => {
         throw  err;
     })
     );
  }

  getSingleProblem(problemId) {
    return this.httpClient.get(this.APIurl + 'problems/user/' + problemId).pipe(map((response: ResponseInterface) => {
        return response.data;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      shareReplay(),
      catchError(err => {
        throw err;
      }));
  }

  uploadProblemFiles(problemId, files) {
    const params = new HttpParams()
      .set('problemId', problemId);
    const formData = new FormData();
    formData.append('files', files);
    return this.httpClient.put(this.APIurl + 'problems/images/', formData, {params}).pipe(map((response: ResponseInterface) => {
        return response.data;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      shareReplay(),
      catchError(err => {
        throw err;
      }));
  }

}
