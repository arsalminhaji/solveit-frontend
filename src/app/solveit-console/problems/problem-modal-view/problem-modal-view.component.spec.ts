import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemModalViewComponent } from './problem-modal-view.component';

describe('ProblemModalViewComponent', () => {
  let component: ProblemModalViewComponent;
  let fixture: ComponentFixture<ProblemModalViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemModalViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemModalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
