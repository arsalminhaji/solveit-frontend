import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {ProblemsService} from '../problems.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-problem-modal-view',
  templateUrl: '/problem-modal-view.component.html',
  styleUrls: ['./problem-modal-view.component.scss']
})
export class ProblemModalViewComponent implements OnInit {

  problemId: string;
  problemView$: Observable<any>;
  constructor(public bsModalRef: BsModalRef, private readonly problemsService: ProblemsService) { }

  ngOnInit() {
    this.viewProblemDataWithSession();
  }
  viewProblemDataWithSession() {
   this.problemView$ =  this.problemsService.getSingleProblem(this.problemId);
  }
}
