import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationAndSkillComponent } from './education-and-skill.component';

describe('EducationAndSkillComponent', () => {
  let component: EducationAndSkillComponent;
  let fixture: ComponentFixture<EducationAndSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationAndSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationAndSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
