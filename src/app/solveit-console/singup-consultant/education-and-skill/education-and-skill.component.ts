import {Component, OnInit, Input} from '@angular/core';
import {FormGroup, FormBuilder, FormArray} from '@angular/forms';
import {SingupConsultantService} from "../singup-consultant.service";

@Component({
  selector: 'app-education-and-skill',
  templateUrl: './education-and-skill.component.html',
  styleUrls: ['./education-and-skill.component.scss']
})
export class EducationAndSkillComponent implements OnInit {
  @Input() heading: string;
  @Input() textBox1PlaceHolder: string;
  @Input() textBox2PlaceHolder: string;
  @Input() config: any = null;
  form: FormGroup;

  constructor(private fb: FormBuilder, private signUpConsultantService: SingupConsultantService) {
    this.form = this.fb.group({
      data: this.fb.array([]),
    })
  }

  data(): FormArray {
    return this.form.get("data") as FormArray;
  }

  years(): string[] {
    let currentYear = new Date().getFullYear();
    let years = [];
    for (let year = currentYear; year >= 1920; year--) {
      years.push(year);
    }
    return years;
  }

  newData(a, b, c, d): FormGroup {
    return this.fb.group({
      textBox1: a,
      textBox2: b,
      startingYear: c,
      endingYear: d
    });
  }

  populateData(records): void {
    for(let data in records){
      if(this.heading === "Accomplish & Certificate"){
        this.data().push(this.newData(records[data]["certification_name"], records[data]["certification_link"],
          records[data]["starting_year"], records[data]["ending_year"]));
        this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      } else if (this.heading === "Work Experience") {
        this.data().push(this.newData(records[data]["organization_name"], records[data]["role"],
          records[data]["starting_year"], records[data]["ending_year"]));
        this.signUpConsultantService.setWorkExperience(this.data().controls);
      } else if (this.heading === "Education") {
        this.data().push(this.newData(records[data]["degree_name"], records[data]["institution_name"],
          records[data]["starting_year"], records[data]["ending_year"]));
        this.signUpConsultantService.setEducationDetails(this.data().controls);
      }
    }
  }

  addData(data) {
    this.data().push(this.newData('', '', '', ''));
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
    }
  }

  removeData(i: number, data) {
    this.data().removeAt(i);
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        education: this.signUpConsultantService.getEducation()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        experience: this.signUpConsultantService.getWorkExperience()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        certification: this.signUpConsultantService.getAccomplishAndCertificate()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    }
  }

  onTextBox1Change(data){
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        education: this.signUpConsultantService.getEducation()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        experience: this.signUpConsultantService.getWorkExperience()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        certification: this.signUpConsultantService.getAccomplishAndCertificate()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    }
  }

  onTextBox2Change(data){
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        education: this.signUpConsultantService.getEducation()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        experience: this.signUpConsultantService.getWorkExperience()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        certification: this.signUpConsultantService.getAccomplishAndCertificate()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    }
  }

  onStartingYearChange(data){
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        education: this.signUpConsultantService.getEducation()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        experience: this.signUpConsultantService.getWorkExperience()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        certification: this.signUpConsultantService.getAccomplishAndCertificate()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    }
  }

  onEndingYearChange(data){
    if (data === "Education") {
      this.signUpConsultantService.setEducationDetails(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        education: this.signUpConsultantService.getEducation()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Work Experience") {
      this.signUpConsultantService.setWorkExperience(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        experience: this.signUpConsultantService.getWorkExperience()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    } else if (data === "Accomplish & Certificate") {
      this.signUpConsultantService.setAccomplishAndCertificate(this.data().controls);
      this.signUpConsultantService.updateConsultantProfile({
        certification: this.signUpConsultantService.getAccomplishAndCertificate()
      }, "DRAFT").subscribe( data => console.log("Draft saved"));
    }
  }

  ngOnInit() {
    console.log('config', this.config);
    this.populateData(this.config);
  }

}
