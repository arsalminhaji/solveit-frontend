import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Global} from "../../global";

@Injectable({
  providedIn: 'root'
})
export class SingupConsultantService {

  educationDetails: Object[];
  workExperience: Object[];
  accomplishAndCertificate: Object[];
  skills: string[];
  about: string;
  private readonly apiUrl: string;

  constructor(private httpClient: HttpClient, private global: Global) {
    this.apiUrl = global.url;
  }

  addConsultantProfile(about, educationDetails, workExperience, skill, accomplishCertificate) {
  }

  setEducationDetails(_educationDetails) {
    let data = [];
    for (let i = 0; i < _educationDetails.length; i++) {
      data.push(
        {
          "ending_year": _educationDetails[i].controls.endingYear.value,
          "starting_year": _educationDetails[i].controls.startingYear.value,
          "institution_name": _educationDetails[i].controls.textBox1.value,
          "degree_name": _educationDetails[i].controls.textBox2.value
        }
      );
    }
    this.educationDetails = data;
    console.log(this.educationDetails);
  }

  setWorkExperience(_workExperience) {
    let data = [];
    for (let i = 0; i < _workExperience.length; i++) {
      data.push(
        {
          "ending_year": _workExperience[i].controls.endingYear.value,
          "starting_year": _workExperience[i].controls.startingYear.value,
          "organization_name": _workExperience[i].controls.textBox1.value,
          "role": _workExperience[i].controls.textBox2.value
        }
      );
    }
    this.workExperience = data;
    console.log(this.workExperience);
  }

  setAccomplishAndCertificate(_accomplishAndCertificate) {
    let data = [];
    console.log(_accomplishAndCertificate);
    for (let i = 0; i < _accomplishAndCertificate.length; i++) {
      data.push(
        {
          "ending_year": _accomplishAndCertificate[i].controls.endingYear.value,
          "starting_year": _accomplishAndCertificate[i].controls.startingYear.value,
          "certification_name": _accomplishAndCertificate[i].controls.textBox1.value,
          "certification_link": _accomplishAndCertificate[i].controls.textBox2.value
        }
      );
    }
    this.accomplishAndCertificate = data;
    console.log(this.accomplishAndCertificate);
  }

  setSkills(_skills) {
    this.skills = _skills;
    console.log(this.skills);
  }

  setAbout(_about) {
    this.about = _about;
    console.log(this.about)
  }

  getAbout() {
    return this.about;
  }

  getWorkExperience() {
    return this.workExperience;
  }

  getSkills() {
    return this.skills;
  }

  getAccomplishAndCertificate() {
    return this.accomplishAndCertificate;
  }

  getEducation() {
    return this.educationDetails;
  }

  isEducationValid(){
    return this.getEducation() ? this.getEducation().length > 1 : false;
  }

  isWorkExperienceValid(){
    return this.getWorkExperience() ? this.getWorkExperience().length > 0 : false;
  }

  isCertificationValid(){
    return true;
  }

  isSkillValid(){
    return this.getSkills() ? this.getSkills().length > 0 : false;
  }

  isAboutValid(){
    return this.getAbout() ? this.getAbout().length > 50 : false;
  }

  getConsultantProfile() {
    return this.httpClient.get(this.apiUrl + 'consultant-profiles/get-profile');
  }

  updateConsultantProfile(requestBody, profileStatus) {
    return this.httpClient.put(this.apiUrl + 'consultant-profiles?profile_status=' + profileStatus, requestBody);
  }

}
