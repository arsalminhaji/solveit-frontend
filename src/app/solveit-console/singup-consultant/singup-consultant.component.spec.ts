import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingupConsultantComponent } from './singup-consultant.component';

describe('SingupConsultantComponent', () => {
  let component: SingupConsultantComponent;
  let fixture: ComponentFixture<SingupConsultantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingupConsultantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingupConsultantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
