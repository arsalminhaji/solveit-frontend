import { Component, OnInit } from '@angular/core';
import {SingupConsultantService} from "./singup-consultant.service";
import {Observable} from "rxjs";
import {async} from "rxjs/internal/scheduler/async";

@Component({
  selector: 'app-singup-consultant',
  templateUrl: './singup-consultant.component.html',
  styleUrls: ['./singup-consultant.component.scss']
})
export class SingupConsultantComponent implements OnInit {

  educationHeading: string = "Education";
  workExperienceHeading: string = "Work Experience";
  accomplishAndCertificateHeading: string = "Accomplish & Certificate";
  certificationName: string = "Certification Name";
  certificationLink: string = "Certification Link";
  degreeName: string = "Degree Name";
  institutionName: string = "Institution Name";
  organizationName: string = "Organization Name";
  role: string = "Role";
  skills: string[] = [];
  consultantProfile: Observable<any>;
  aboutText: string = "";
  educations: [] = [];
  experience: [] = [];
  certification: [] = [];
  dataLoaded: Promise<boolean>;

  constructor(private _signUpConsultantService: SingupConsultantService) { }

  ngOnInit() {
    console.log(this.certification);
    this.consultantProfile = this._signUpConsultantService.getConsultantProfile();
    this.consultantProfile.subscribe(
      data => {
        this.aboutText = data ? data.about : null;
        this._signUpConsultantService.setAbout(data.about);
        this.skills = data ? data.skill : null;
        this._signUpConsultantService.setSkills(data.skill);
        this.educations = data ? data.education : null;
        this.experience = data ? data.experience : null;
        this.certification = data ? data.certification : null;
        this.dataLoaded = Promise.resolve(true);
      }
    );
  }

  onSkillClicked(){
    document.getElementById("skill-textbox").focus();
  }

  onEnter(skillInput){
    this.skills.push(skillInput.value);
    skillInput.value = "";
    this._signUpConsultantService.setSkills(this.skills);
    this._signUpConsultantService.updateConsultantProfile({
      skill: this.skills
    }, "DRAFT").subscribe(
      data => console.log("Draft Saved")
    );
  }

  onBackSpace(skillInput){
    if(!skillInput.value){
      this.skills.pop();
    }
    this._signUpConsultantService.setSkills(this.skills);
    this._signUpConsultantService.updateConsultantProfile({
      skill: this.skills
    }, "DRAFT").subscribe(
      data => console.log("Draft Saved")
    );
  }

  onAboutChange(about){
    this._signUpConsultantService.setAbout(about);
    this._signUpConsultantService.updateConsultantProfile({
      about: about
    }, "DRAFT").subscribe(
      data => console.log("Draft Saved")
    );
  }

  isSubmitDisabled(){
    return !(this._signUpConsultantService.isAboutValid() && this._signUpConsultantService.isEducationValid() &&
      this._signUpConsultantService.isWorkExperienceValid() && this._signUpConsultantService.isCertificationValid() &&
      this._signUpConsultantService.isSkillValid());
  }

  onFormSubmit(){
    console.log("submitted");
    this._signUpConsultantService.updateConsultantProfile({
      about: this._signUpConsultantService.getAbout(),
      education: this._signUpConsultantService.getEducation(),
      experience: this._signUpConsultantService.getWorkExperience(),
      certification: this._signUpConsultantService.getAccomplishAndCertificate(),
      skill: this._signUpConsultantService.getSkills()
    }, "PENDING").subscribe(data => alert("Submitted"));
  }

}
