export interface SingupConsultant {
  "about": string,
  "education": [{
    "degreeName": string,
    "institutionName": string,
    "startingYear": string,
    "endingYear": string
  }],
  "workExperience": [{
    "organizationName": string,
    "role": string,
    "startingYear": string,
    "endingYear": string
  }],
  "skill": string[],
  accomplishAndCertificate: string[]
}
