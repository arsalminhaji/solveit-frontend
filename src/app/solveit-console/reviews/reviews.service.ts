import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Global} from '../../global';
import {ResponseInterface} from '../../interface/sucess.response.interface';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';




@Injectable({
  providedIn: 'root'
})

export class ReviewsService {
  private apiUrl;
  constructor(private httpclient: HttpClient , private global: Global) {
    this.apiUrl = global.url;
  }
     getReviews(): Observable<any> {
       return this.httpclient.get(this.apiUrl + 'reviews');

     }

     addReviews(): Observable<any> {
     // let postReviews;
       const reviewsObject = {
       url: this.apiUrl + 'reviews',
       body: null
       };
       return  this.httpclient.post(reviewsObject.url, reviewsObject.body);


   }
   deleteReviews(): Observable<any> {
     return this.httpclient.delete(this.apiUrl + 'reviews');

   }

  // getComments(): Observable<any> {
  //   return this.httpclient.get('https://jsonplaceholder.typicode.com/posts/1/comments');
  // }
  // getCommentsByParameter(): Observable<any> {
  //   const params1 = new HttpParams().set('userId', '1');
  //   return this.httpclient.get('https://jsonplaceholder.typicode.com/users/1/posts', {params: params1});

  // }
  // post(oPost: Posts ): Observable<any> {
  //   return this.httpclient.post('https://jsonplaceholder.typicode.com/posts', oPost);
}
