import { Injectable } from '@angular/core';
import {CommonService} from '../../common.service';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {BehaviorSubject} from 'rxjs';
import {catchError, filter, map, tap} from 'rxjs/operators';
import {ResponseInterface} from '../../interface/sucess.response.interface';
import {Problem} from '../problems/problem';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private APIurl;
  private currentUserType;
  detectChangeSubject = new  BehaviorSubject(null);
  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService) {
    this.APIurl = global.url;
    this.commonService.globalCurrentUserTypeLocal.subscribe((usertype) => {
      if (usertype == null) {
        // tslint:disable-next-line:radix
        this.currentUserType = parseInt(this.commonService.returnCurrentUserType());
        this.detectChangeSubject.next(this.currentUserType);
      } else {
        this.currentUserType = usertype;
        this.detectChangeSubject.next(usertype);
      }
    });
  }

  loadPublicProblemsToSolveAccordingToConsultantTags() {
    return this.httpClient.get(this.APIurl + 'dashboard/consultant/tags').pipe(map((response: ResponseInterface) => {
        // return  response.data.dashboardList.map((filteredUserProblem) => {
        // });
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }

  /* TODO can be latest problems , can be most viewed problems ,can be most requested problems and can be combination off all atleast 20 */
  loadPublicProblemsToViewAccordingToNoTags() {
    return this.httpClient.get(this.APIurl + 'dashboard/user/view/notags').pipe(map((response: ResponseInterface) => {
        // return  response.data.filteredUserProblems.map((filteredUserProblem) => {
        // });
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }

  loadPublicProblemSolverProfileWithSkillTags() {
    return this.httpClient.get(this.APIurl + 'dashboard/user/problemsolver/tags').pipe(map((response: ResponseInterface) => {
        // return  response.data.filteredUserProblems.map((filteredUserProblem) => {
        // });
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }
  loadPublicProblemToViewByTags() {
    return this.httpClient.get(this.APIurl + 'dashboard/user/problemsolver/byTags').pipe(map((response: ResponseInterface) => {

      // return response.data.filteredUserProblem.map((filteredUserProblem) => {

      // });
    }),
    tap((mappedData) => {console.log(mappedData); }),
    catchError(err => {
      throw err;
    }));

  }

}
