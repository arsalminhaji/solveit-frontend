import { Component, OnInit } from '@angular/core';
import {DashboardService} from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private dashboardService: DashboardService) { }

  problemViewAsUser$;
  problemToViewAsSolver$;
  consultantProfileForPrivateProblemList$;
  dashboardLocalSubject: any;
  loadRequiredUserDashboard;
  ngOnInit() {
    this.dashboardLocalSubject = this.dashboardService.detectChangeSubject.subscribe((value) => {
      this.loadRequiredUserDashboard = value;
      if (value) {
        this.showAndLoadDashboardAsProblemSolver();
      } else {
        console.log(value);
        this.showAndLoadDashboardAsUser();
      }
    });
  }


  showAndLoadDashboardAsUser() {
    this.problemViewAsUser$ = this.dashboardService.loadPublicProblemsToViewAccordingToNoTags();
    this.consultantProfileForPrivateProblemList$ = this.dashboardService.loadPublicProblemSolverProfileWithSkillTags();
  }

  showAndLoadDashboardAsProblemSolver() {
    this.problemToViewAsSolver$ = this.dashboardService.loadPublicProblemsToSolveAccordingToConsultantTags();
  }

  sendRequestToSolve() {

  }
  showPublicProblemToViewByTags() {
    this.dashboardService.loadPublicProblemToViewByTags()
    .subscribe(
      d => {
        alert('showPublicProblemToViewByTags');

      }
    );

  }
  showPublicProblemSolverProfileWithSkillTags() {
    this.dashboardService.loadPublicProblemSolverProfileWithSkillTags()
    .subscribe(
      d => {
        alert('showPublicProblemSolverProfileWithSkillTags');
      }
    );
  }
  showPublicProblemsToViewAccordingToNoTags() {
    this.dashboardService.loadPublicProblemsToViewAccordingToNoTags()
    .subscribe(
      d => {
        alert('loadPublicProblemsToViewAccordingToNoTags');
      }
    );
  }
  showPublicProblemsToSolveAccordingToConsultantTags() {
    this.dashboardService.loadPublicProblemsToSolveAccordingToConsultantTags()
    .subscribe(
      d => {
        alert('loadPublicProblemsToSolveAccordingToConsultantTags');
      }
    );
  }

}
