export interface UserSetting {
  "username": string,
  "email": string,
  "password": string,
  "phoneNumber": string,
  userSettings: {
    twoWayFactor: boolean,
    languages: string[],
    country: string
  }
  "communicationSetting": {
    sessionRequest: string[],
    sessionRequestAcceptance: string[],
    problemApproval: string[],
    sessionReminder: string[],
    consultantProfileApproval: string[]
  }
}
