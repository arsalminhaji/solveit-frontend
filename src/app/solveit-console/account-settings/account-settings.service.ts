import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Global} from '../../global';
import {UserSetting} from "./userSettingInterface";
import {catchError, map, shareReplay} from "rxjs/operators";
import {ResponseInterface} from "../../interface/sucess.response.interface";
import {Observable, of, throwError} from "rxjs";
import {ErrorResponseInterface} from "../../interface/error.response.interface";

@Injectable({
  providedIn: 'root'
})
export class AccountSettingsService {
  private readonly apiUrl: string;
  private errorObject: ErrorResponseInterface;

  constructor(private httpClient: HttpClient, private global: Global) {
    this.apiUrl = global.url;
  }

  getUserSettingData(): Observable<UserSetting> {
    return this.httpClient.get<ResponseInterface>(this.apiUrl + 'users/settings/get-current-user-settings')
      .pipe(map ((response: ResponseInterface) => {
        return response.data;
      }, shareReplay()));
  }

  updateUsername(username: string): Observable<ResponseInterface>{
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/username', {
      "username": username
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    )
  }

  updateEmail(email: string): Observable<ResponseInterface>{
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/email', {
      "email": email
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updatePassword(oldPassword: string, newPassword: string){
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/password', {
      "oldPassword": oldPassword,
      "newPassword": newPassword
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updatePhoneNumber(phoneNumber: string){
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/phone-number', {
      "phoneNumber": phoneNumber
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateTwoFactor(twoWayAuthentication: boolean){
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/two-way-authentication', {
      "twoWayAuthentication": twoWayAuthentication
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateCountry(country: string){
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/country', {
      "country": country
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateLanguage(language: string){
    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/language', {
      "language": language
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateSessionRequest(sms: boolean, email: boolean, pushNotification: boolean){
    let responseBody = [];
    if(sms) responseBody.push("SMS");
    if(email) responseBody.push("EMAIL");
    if(pushNotification) responseBody.push("PUSH_NOTIFICATIONS");

    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/communication-setting/session-request', {
      "sessionRequest": responseBody
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateSessionRequestAcceptance(sms: boolean, email: boolean, pushNotification: boolean){
    let responseBody = [];
    if(sms) responseBody.push("SMS");
    if(email) responseBody.push("EMAIL");
    if(pushNotification) responseBody.push("PUSH_NOTIFICATIONS");

    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/communication-setting/session-request-acceptance', {
      "sessionRequestAcceptance": responseBody
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateProblemApproval(sms: boolean, email: boolean, pushNotification: boolean){
    let responseBody = [];
    if(sms) responseBody.push("SMS");
    if(email) responseBody.push("EMAIL");
    if(pushNotification) responseBody.push("PUSH_NOTIFICATIONS");

    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/communication-setting/problem-approval', {
      "problemApproval": responseBody
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateSessionRemainder(sms: boolean, email: boolean, pushNotification: boolean){
    let responseBody = [];
    if(sms) responseBody.push("SMS");
    if(email) responseBody.push("EMAIL");
    if(pushNotification) responseBody.push("PUSH_NOTIFICATIONS");

    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/communication-setting/session-reminder', {
      "sessionReminder": responseBody
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

  updateConsultantProfileApproval(sms: boolean, email: boolean, pushNotification: boolean){
    let responseBody = [];
    if(sms) responseBody.push("SMS");
    if(email) responseBody.push("EMAIL");
    if(pushNotification) responseBody.push("PUSH_NOTIFICATIONS");

    return this.httpClient.patch<ResponseInterface>(this.apiUrl + 'users/settings/communication-setting/consultant-profile-approval', {
      "consultantProfileApproval": responseBody
    }).pipe(
      catchError(err => throwError(err)),
      shareReplay()
    );
  }

}
