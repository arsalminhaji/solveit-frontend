import { Component, OnInit } from '@angular/core';
import { AccountSettingsService } from "./account-settings.service";
import { UserSetting } from "./userSettingInterface";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {ResponseInterface} from "../../interface/sucess.response.interface";
import {ErrorResponseInterface} from "../../interface/error.response.interface";
import {HttpErrorResponse} from "@angular/common/http";
import {SettingsUIEnum} from '../../common/enums';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  userSetting: Observable<UserSetting>;
  responseObject: Observable<ResponseInterface>;
  errorObject: ErrorResponseInterface;
  usernameEditBtnEnabled: boolean = true;
  emailEditBtnEnabled: boolean = true;
  passwordBtnEnabled: boolean = true;
  phoneNumberEnabled: boolean = true;
  isUsernameUpdatedSuccessfully: boolean = false;
  isEmailUpdatedSuccessfully: boolean = false;
  isUsernameUpdatedError: boolean = false;
  isEmailUpdatedError: boolean = false;
  isPasswordUpdatedSuccessfully: boolean = false;
  isPasswordUpdatedError: boolean = false;
  isPhoneNumberUpdatedError: boolean = false;
  isPhoneNumberUpdatedSuccessfully: boolean = false;
  isTwoFactorUpdatedSuccessfully: boolean = false;
  isTwoFactorUpdatedError: boolean = false;
  isCountryUpdatedError: boolean = false;
  isCountryUpdatedSuccessfully: boolean = false;
  isLanguageUpdatedError: boolean = false;
  isLanguageUpdatedSuccessfully: boolean = false;
  isSessionRequestUpdatedError: boolean = false;
  isSessionRequestUpdatedSuccessfully: boolean = false;
  isSessionRequestAcceptanceUpdatedError: boolean = false;
  isSessionRequestAcceptanceUpdatedSuccessfully: boolean = false;
  isProblemApprovalUpdatedError: boolean = false;
  isProblemApprovalUpdatedSuccessfully: boolean = false;
  isSessionRemainderUpdatedSuccessfully: boolean = false;
  isSessionRemainderUpdatedError: boolean = false;
  isConsultantProfileApprovalUpdatedError: boolean = false;
  isConsultantProfileApprovalUpdatedSuccessfully: boolean = false;
  settingEnum = SettingsUIEnum
  userSettingActiveState: string = SettingsUIEnum.PREFRENCES;

  constructor(private accountSettingService: AccountSettingsService) {}

  ngOnInit() {
    this.initUserSetting();
  }

  settingUIChange(UIStateChange) {
    this.userSettingActiveState = UIStateChange;
  }
  onUsernameEditClicked(){
    this.usernameEditBtnEnabled = false;
  }

  onEmailEditClicked(){
    this.emailEditBtnEnabled = false;
  }

  onPasswordEditClicked(){
    this.passwordBtnEnabled = false;
  }

  onPhoneNumberEditClicked(){
    this.phoneNumberEnabled = false;
  }

  initUserSetting(){
    this.userSetting = this.accountSettingService.getUserSettingData();
  }

   onUsernameUpdateBtnClicked(username: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateUsername(username.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isUsernameUpdatedError = true;
        throw err;
      })
    );
     this.isUsernameUpdatedSuccessfully = true;
     this.resetErrorAndSuccessfulMessage();
  }

  onEmailUpdateBtnClicked(email: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateEmail(email.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isEmailUpdatedError = true;
        throw err;
      })
    );
    this.isEmailUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onPasswordUpdateBtnClicked(oldPassword: HTMLInputElement, newPassword: HTMLInputElement){
    this.responseObject = this.accountSettingService.updatePassword(oldPassword.value, newPassword.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isPasswordUpdatedError = true;
        throw err;
      })
    );
    this.isPasswordUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onPhoneNumberBtnClicked(phoneNumber: HTMLInputElement){
    this.responseObject = this.accountSettingService.updatePhoneNumber(phoneNumber.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isPhoneNumberUpdatedError = true;
        throw err;
      })
    );
    this.isPhoneNumberUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onTwoFactorBtnClicked(twoFactor: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateTwoFactor(twoFactor.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isTwoFactorUpdatedError = true;
        throw err;
      })
    );
    this.isTwoFactorUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onCountryBtnClicked(country: HTMLSelectElement){
    this.responseObject = this.accountSettingService.updateCountry(country.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isCountryUpdatedError = true;
        throw err;
      })
    );
    this.isCountryUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onLanguageBtnClicked(language: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateLanguage(language.value).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isLanguageUpdatedError = true;
        throw err;
      })
    );
    this.isLanguageUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onSessionRequestBtnClicked(sms: HTMLInputElement, email: HTMLInputElement, pushNotification: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateSessionRequest(sms.checked,email.checked, pushNotification.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isSessionRequestUpdatedError = true;
        throw err;
      })
    );
    this.isSessionRequestUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onSessionRequestAcceptanceBtnClicked(sms: HTMLInputElement, email: HTMLInputElement, pushNotification: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateSessionRequestAcceptance(sms.checked,email.checked, pushNotification.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isSessionRequestAcceptanceUpdatedError = true;
        throw err;
      })
    );
    this.isSessionRequestAcceptanceUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onProblemApprovalBtnClicked(sms: HTMLInputElement, email: HTMLInputElement, pushNotification: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateProblemApproval(sms.checked,email.checked, pushNotification.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isProblemApprovalUpdatedError = true;
        throw err;
      })
    );
    this.isProblemApprovalUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onSessionReminderBtnClicked(sms: HTMLInputElement, email: HTMLInputElement, pushNotification: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateSessionRemainder(sms.checked,email.checked, pushNotification.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isSessionRemainderUpdatedError = true;
        throw err;
      })
    );
    this.isSessionRemainderUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  onConsultantProfileApprovalBtnClicked(sms: HTMLInputElement, email: HTMLInputElement, pushNotification: HTMLInputElement){
    this.responseObject = this.accountSettingService.updateConsultantProfileApproval(sms.checked,email.checked, pushNotification.checked).pipe(
      catchError((err: HttpErrorResponse) => {
        this.setErrorObject(err);
        this.isConsultantProfileApprovalUpdatedError = true;
        throw err;
      })
    );
    this.isConsultantProfileApprovalUpdatedSuccessfully = true;
    this.resetErrorAndSuccessfulMessage();
  }

  resetErrorAndSuccessfulMessage(){
    setTimeout(() => {
      this.errorObject = null;
      this.responseObject = null;
      this.isConsultantProfileApprovalUpdatedSuccessfully = false;
      this.isConsultantProfileApprovalUpdatedError = false;
      this.isSessionRemainderUpdatedSuccessfully = false;
      this.isSessionRemainderUpdatedError = false;
      this.isProblemApprovalUpdatedSuccessfully = false;
      this.isProblemApprovalUpdatedError = false;
      this.isSessionRequestAcceptanceUpdatedSuccessfully = false;
      this.isSessionRequestAcceptanceUpdatedError = false;
      this.isSessionRequestUpdatedSuccessfully = false;
      this.isSessionRequestUpdatedError = false;
      this.isLanguageUpdatedSuccessfully = false;
      this.isLanguageUpdatedError = false;
      this.isCountryUpdatedSuccessfully = false;
      this.isCountryUpdatedError = false;
      this.isTwoFactorUpdatedSuccessfully = false;
      this.isTwoFactorUpdatedError = false;
      this.isPhoneNumberUpdatedSuccessfully = false;
      this.isPhoneNumberUpdatedError = false;
      this.isPasswordUpdatedSuccessfully = false;
      this.isPasswordUpdatedError = false;
      this.isEmailUpdatedSuccessfully = false;
      this.isEmailUpdatedError = false;
      this.isUsernameUpdatedSuccessfully = false;
      this.isUsernameUpdatedError = false;
    }, 2000);
  }

  setErrorObject(err){
    this.errorObject = {
      "statusCode": err.status,
      "message": err.error.message,
      "error": err.error.error
    };
  }

}
