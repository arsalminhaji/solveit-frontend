import {Component, OnInit, ViewChild} from '@angular/core';
import {AgoraClient, ClientEvent, NgxAgoraService, Stream, StreamEvent} from 'ngx-agora';
import {CdkPortal} from '@angular/cdk/portal';
import {LiveCallService} from './live-call.service';
import {ActivatedRoute} from '@angular/router';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-live-call',
  templateUrl: './live-call.component.html',
  styleUrls: ['./live-call.component.scss']
})
export class LiveCallComponent implements OnInit {

  private externalWindow = null;
  private client: AgoraClient;
  private localStream: Stream;

  constructor(private ngxAgoraService: NgxAgoraService, private liveCallService: LiveCallService , private activateRoute: ActivatedRoute) {

  }
  title = 'angular-video';
  localCallId = null;
  remoteCalls: string[] = [];
  ngOnInit() {
    // this.externalWindow = window.open('', '', 'width=1000,height=500,left=200,top=180');
   this.activateRoute.queryParams.subscribe((queryParam) => {
     this.startOrJoinCall(queryParam);
   });
  }


  startOrJoinCall(queryParam) {
    console.log(queryParam);
    this.liveCallService.startCallAndJoinCallVerifySession(queryParam).subscribe((clientAgoraInfo) =>{
      const streamUUId = uuid();
      this.createClientInit(streamUUId, clientAgoraInfo);
    }, error => {
      console.log(error)
    });
  }

  createClientInit(streamUUID, clientAgoraInfo) {
    console.log(clientAgoraInfo);
    this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
    this.assignClientHandlers();
    this.localStream = this.ngxAgoraService.createStream({ streamID: streamUUID, audio: true, video: true, screen: false });
    this.assignLocalStreamHandlers();
    this.localCallId =  String(clientAgoraInfo.serverUserUUID);
    this.initLocalStream(() => this.join(clientAgoraInfo.serverUserToken, clientAgoraInfo.channelName, clientAgoraInfo.serverUserUUID,
    ()   => this.publish(), error => console.error(error)));
  }
  private getRemoteId(stream: Stream): string {
    return `agora_remote-${stream.getId()}`;
  }
  /**
   * Attempts to connect to an online chat room where users can host and receive A/V streams.
   */
  join(serverUserToken, userChannelName, serverUserUUID, onSuccess?: () => void, onFailure?: (error: Error) => void): void {
    this.client.join(serverUserToken, userChannelName, serverUserUUID , onSuccess, onFailure);
  }

  /**
   * Attempts to upload the created local A/V stream to a joined chat room.
   */
  publish(): void {
    this.client.publish(this.localStream, err => console.log('Publish local stream error: ' + err));
  }

  private assignLocalStreamHandlers(): void {
    this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });

    // The user has denied access to the camera and mic.
    this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });
  }

  private initLocalStream(onSuccess?: () => any): void {
    this.localStream.init(
      () => {
        // The user has granted access to the camera and mic.
        this.localStream.play(this.localCallId);
        if (onSuccess) {
          onSuccess();
        }
      },
      err => console.error('getUserMedia failed', err)
    );
  }

  private assignClientHandlers(): void {
    this.client.on(ClientEvent.LocalStreamPublished, evt => {
      console.log('Publish local stream successfully');
    });

    this.client.on(ClientEvent.Error, error => {
      console.log('Got error msg:', error.reason);
      if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.client.renewChannelKey(
          'chanelchec',
          () => console.log('Renewed the channel key successfully.'),
          renewError => console.error('Renew channel key failed: ', renewError)
        );
      }
    });

    this.client.on(ClientEvent.RemoteStreamAdded, evt => {
      const stream = evt.stream as Stream;
      this.client.subscribe(stream, { audio: true, video: true }, err => {
        console.log('Subscribe stream failed', err);
      });
    });

    this.client.on(ClientEvent.RemoteStreamSubscribed, evt => {
      const stream = evt.stream as Stream;
      const id = this.getRemoteId(stream);
      if (!this.remoteCalls.length) {
        this.remoteCalls.push(id);
        setTimeout(() => stream.play(id), 1000);
      }
    });

    this.client.on(ClientEvent.RemoteStreamRemoved, evt => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = [];
        console.log(`Remote stream is removed ${stream.getId()}`);
      }
    });

    this.client.on(ClientEvent.PeerLeave, evt => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
        console.log(`${evt.uid} left from this channel`);
      }
    });
  }

}
