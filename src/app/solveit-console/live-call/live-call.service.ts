import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Global} from '../../global';
import {CommonService} from '../../common.service';
import {NgxAgoraService} from 'ngx-agora';
import {catchError, map, tap} from 'rxjs/operators';
import {ResponseInterface} from '../../interface/sucess.response.interface';

@Injectable({
  providedIn: 'root'
})
export class LiveCallService {
  private APIurl;

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService) {
    this.APIurl = global.url;

  }
  startCallAndJoinCallVerifySession(queryParams) {
    const params = new HttpParams({ fromObject: queryParams });
    return this.httpClient.get(this.APIurl + 'call-sessions/join', {params}).pipe(map((response: ResponseInterface) => {
      return response.data.agora_token;
    }),
    tap((tokenData) => {
      console.log(tokenData);
    }),
    catchError((err) => {
      throw err;
    }));
  }
}
