import { TestBed } from '@angular/core/testing';

import { LiveCallService } from './live-call.service';

describe('LiveCallService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LiveCallService = TestBed.get(LiveCallService);
    expect(service).toBeTruthy();
  });
});
