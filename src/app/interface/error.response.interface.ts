export interface ErrorResponseInterface {
  statusCode: number;
  message: string;
  error: any;
}
