import {CustomAuthService} from './auth/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule,
  MatButtonModule, MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatDialogModule, MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule, MatNativeDateModule, MatPaginatorModule,
  MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule,
  MatSelectModule,
  MatSidenavModule, MatSliderModule,
  MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatStepperModule,
  MatTableModule, MatTabsModule,
  MatToolbarModule,
  MatTooltipModule, MatTreeModule
} from '@angular/material';
import {A11yModule} from '@angular/cdk/a11y';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTreeModule} from '@angular/cdk/tree';
import {CdkTableModule} from '@angular/cdk/table';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {OverlayModule} from '@angular/cdk/overlay';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {PortalModule} from '@angular/cdk/portal';
import { DashboardComponent } from './solveit-console/dashboard/dashboard.component';
import { ProblemsComponent } from './solveit-console/problems/problems.component';
import { SessionsComponent } from './solveit-console/sessions/sessions.component';
import { UserAuthComponent } from './auth/user-auth/user-auth.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { AccountSettingsComponent } from './solveit-console/account-settings/account-settings.component';
import { ErrorResponseComponent } from './common-component/error-response/error-response.component';
import { SucessResponseComponent } from './common-component/sucess-response/sucess-response.component';
import { LiveCallComponent } from './solveit-console/live-call/live-call.component';
import { SearchComponent } from './common-component/search/search.component';
import { HomeComponent } from './solveit-website/home/home.component';
import {FooterComponent} from './common-component/footer/footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ConsoleIndexComponent } from './solveit-console/console-index/console-index.component';
import { SingupConsultantComponent } from './solveit-console/singup-consultant/singup-consultant.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { JwtInterceptor} from './interceptors/jwt.interceptor';
import {Global} from './global';
import { UserSwitchComponent } from './common-component/user-switch/user-switch.component';
import { PostPropblemComponent } from './solveit-console/problems/post-propblem/post-propblem.component';
import {QuillModule} from 'ngx-quill';
import { environment } from 'src/environments/environment';
import { NgxAgoraModule } from 'ngx-agora';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {FcmMessagingService} from './common/fcm-messaging.service';
import { StaticHeaderComponent } from './solveit-website/static-header/static-header.component';
import { EducationAndSkillComponent } from './solveit-console/singup-consultant/education-and-skill/education-and-skill.component';
import { ViewProblemComponent } from './solveit-console/problems/view-problem/view-problem.component';
import { ProblemModalViewComponent } from './solveit-console/problems/problem-modal-view/problem-modal-view.component';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import {ErrorInterceptor} from './interceptors/error.interceptor';
// import {ReviewsService} from './solveit-console/reviews/reviews.service';
import {ReviewsComponent} from './solveit-console/reviews/reviews.component';
import {NgxUploaderModule} from 'ngx-uploader';
import {appInitializer} from './helper/fb';
import {getAuthServiceConfigs} from './helper/authServiceConfig';
import {AuthServiceConfig, SocialLoginModule} from 'angularx-social-login';
import {SocialLoginService} from './common/social-login.service';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        ProblemsComponent,
        SessionsComponent,
        UserAuthComponent,
        ForgotPasswordComponent,
        AccountSettingsComponent,
        ErrorResponseComponent,
        SucessResponseComponent,
        LiveCallComponent,
        SearchComponent,
        HomeComponent,
        FooterComponent,
        FooterComponent,
        ConsoleIndexComponent,
        SingupConsultantComponent,
        UserSwitchComponent,
        PostPropblemComponent,
        StaticHeaderComponent,
        EducationAndSkillComponent,
        ViewProblemComponent,
        ProblemModalViewComponent,
        ReviewsComponent
    ],
  providers: [SocialLoginService,
    {  provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { provide: APP_INITIALIZER, useFactory: appInitializer, deps: [SocialLoginService] , multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
    FcmMessagingService,
    BsModalService,
    Global,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ModalModule.forRoot(),
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatToolbarModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    OverlayModule,
    PortalModule,
    ScrollingModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
    NgxAgoraModule.forRoot({AppID: environment.agora.appId}),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgxUploaderModule,
    SocialLoginModule
  ],
  entryComponents: [
    ProblemModalViewComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
