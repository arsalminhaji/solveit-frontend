import {CustomAuthService} from '../auth/auth.service';
import {environment} from '../../environments/environment';
import {SocialLoginService} from '../common/social-login.service';

declare const FB: any;


export function appInitializer(socialLoginService: SocialLoginService) {
  console.log(socialLoginService);
  return () => new Promise(resolve => {
    // wait for facebook sdk to initialize before starting the angular app
    window['fbAsyncInit'] = function () {
      FB.init({
        appId: environment.facebookAppId,
        cookie: true,
        xfbml: true,
        version: 'v11.0'
      });
      // auto authenticate with the api if already logged in with facebook
      FB.getLoginStatus(({authResponse}) => {
        if (authResponse) {
          resolve();

          FB.api('/me', 'GET', {fields: 'email,id,name'},function(user) {
            console.log(user);
          });
          socialLoginService.customFacebookLogin(authResponse).subscribe((userLoginResponse) => {
              console.log(userLoginResponse);
            }, error => {
              console.log(error);
            });
        } else {
          resolve();
        }
      });
    };

    // load facebook sdk script
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  });
}
