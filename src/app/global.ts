import { Injectable } from '@angular/core';

@Injectable()
export class Global {
  url = 'http://localhost:3000/';
  deviceType = 'BROWSER';
}
