import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {CommonService} from '../../common.service';

@Injectable({
  providedIn: 'root'
})
export class UserSwitchService {

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService) {
    this.globalUserType = this.commonService.returnUserType();
  }
  private readonly globalUserType: any;
  switchUser(userType) {
    this.commonService.switchUserType(userType);
  }
  returnGlobalUserType() {
    return this.globalUserType;
  }
}
