import { TestBed } from '@angular/core/testing';

import { UserSwitchService } from './user-switch.service';

describe('UserSwitchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserSwitchService = TestBed.get(UserSwitchService);
    expect(service).toBeTruthy();
  });
});
