import {Component, Input, OnInit} from '@angular/core';
import {UserSwitchService} from './user-switch.service';
import {UserType} from '../../common/enums';

@Component({
  selector: 'app-user-switch',
  templateUrl: './user-switch.component.html',
  styleUrls: ['./user-switch.component.scss']
})
export class UserSwitchComponent implements OnInit {

  userType = UserType;
  private consoleIndexComponentUserType: any;

  constructor(private readonly switchUserService: UserSwitchService) {
    this.consoleIndexComponentUserType = this.switchUserService.returnGlobalUserType();
  }

  ngOnInit() {
  }

  switchUserType(userType) {
    this.switchUserService.switchUser(userType);
  }
}
