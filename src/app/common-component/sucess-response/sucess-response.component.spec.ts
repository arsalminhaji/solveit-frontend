import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SucessResponseComponent } from './sucess-response.component';

describe('SucessResponseComponent', () => {
  let component: SucessResponseComponent;
  let fixture: ComponentFixture<SucessResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SucessResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SucessResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
