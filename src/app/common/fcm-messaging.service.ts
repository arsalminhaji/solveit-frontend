import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import { AngularFireMessaging } from '@angular/fire/messaging';
import {CommonService} from '../common.service';
import {HttpClient} from '@angular/common/http';
import {Global} from '../global';
import {PushNotificationBuilder} from './PushNotificationBuilder';
import {catchError, map, tap} from 'rxjs/operators';
import {ResponseInterface} from '../interface/sucess.response.interface';

@Injectable({
  providedIn: 'root'
})
export class FcmMessagingService {
  currentMessage = new BehaviorSubject(null);
  private APIurl: string;
  constructor(private httpClient: HttpClient , private global: Global, private angularFireMessaging: AngularFireMessaging, private commonService: CommonService  ) {

    this.APIurl = global.url;
    this.angularFireMessaging.messages.subscribe(
      (_messaging: AngularFireMessaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      });
  }

  requestPermission() {
      this.angularFireMessaging.requestPermission.subscribe((response) => {
        this.angularFireMessaging.requestToken.subscribe(
          (token) => {
            if (token == this.commonService.getFCMToken()) {
                console.log('dont save token');
            } else {
              this.commonService.saveFCMToken(token);
              this.updateFCMTokenAgainstDevice();
            }
          },
          (err) => {
            console.error('Unable to get permission to notify.', err);
          });
      }, error => {
        if (((this.commonService.getFCMToken() == 'null') ? JSON.parse(this.commonService.getFCMToken()) : this.commonService.getFCMToken()) == null) {
          console.log('dont update db');
        } else {
          this.commonService.saveFCMToken(null);
          this.updateFCMTokenAgainstDevice();
          console.error(error);
        }
      });
    }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload:any) => {
        const NotificationOptions = {
          body: payload.notification.body,
          data: payload.data,
          icon: payload.notification.icon
        };
        const pushNotificaion = new PushNotificationBuilder(payload.notification.body, payload.notification.icon
          , payload.notification.click_action , payload.notification.title);
        navigator.serviceWorker.getRegistration('/firebase-cloud-messaging-push-scope').then(registration => {
          registration.showNotification(payload.notification.title, NotificationOptions);
        });
        this.currentMessage.next(pushNotificaion);
      });
  }

  updateFCMTokenAgainstDevice() {
    const deviceRequest = {
      deviceType: 'BROWSER',
      deviceUniqueToken: this.commonService.getUniqueBrowserUUID(),
      deviceFCMToken: this.commonService.getFCMToken()
    };
    this.httpClient.patch(this.APIurl + 'users/device/fcm-token', deviceRequest).subscribe((response) =>{
      console.log(response);
    }, error => {
      console.log(error);
    });
  }
}
