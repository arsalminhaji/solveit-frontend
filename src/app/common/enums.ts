export enum AuthToggle {
  SIGNUP,
  LOGIN
}
export enum UserType {
  USER,
  PROBLEM_SOLVER
}
export enum problemChangeEnum  {
  DRAFT= 'DRAFT',
  CANCELLED = 'CANCELLED',
  PENDING = 'PENDING'
}
export enum SettingsUIEnum  {
  PREFRENCES= 'PREFRENCES',
  COMMUNICATION_SETTINGS = 'COMMUNICATION_SETTINGS',
  USER_SETTINGS = 'USER_SETTINGS'
}
