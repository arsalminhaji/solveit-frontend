import { Injectable } from '@angular/core';
import {catchError, concatMap, map, tap} from 'rxjs/operators';
import {ResponseInterface} from '../interface/sucess.response.interface';
import {EMPTY, from, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Global} from '../global';
import {CommonService} from '../common.service';
import {FcmMessagingService} from './fcm-messaging.service';

@Injectable({
  providedIn: 'root'
})
export class SocialLoginService {
  private APIurl;

  constructor(private httpClient: HttpClient , private global: Global, private commonService: CommonService, private fcmMessagingService: FcmMessagingService) {
    this.APIurl = global.url;

  }

  customFacebookLogin(accessLoginDetails) {
    console.log('accessLoginDetails', accessLoginDetails);
    return  this.httpClient.post(this.APIurl + 'auth/facebook/login', accessLoginDetails ).pipe(map ((response: ResponseInterface) => {
        const userJson = response.data;
        this.commonService.setUserType(userJson.is_consultant);
        this.commonService.setJWTToken(userJson.token);
        return userJson;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));

  }

  facebookLogin() {
    this.facebookLogiUsingFb()
      .pipe(
        concatMap((facebookAccessTokenResponse) => this.facebookGetPublicProfileAndEmail(facebookAccessTokenResponse)),
        concatMap(facebookAUserInfoResponse => this.customFacebookLogin(facebookAUserInfoResponse)))
      .subscribe((successResponseFromFacebook) => {
        console.log(successResponseFromFacebook);
        // get return url from query parameters or default to home page
        // const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        // this.router.navigateByUrl(returnUrl);
      }, error => {

      });
  }

  facebookGetPublicProfileAndEmail(facebookAccessToken) {
    return from(new Promise<any>(resolve => FB.api('/' + facebookAccessToken.userID, 'get', {fields: 'id,name,email'  }, resolve)))
      .pipe(concatMap(({userInfoFaceBook}) => {
        console.log('userInfo', userInfoFaceBook);
        if (!userInfoFaceBook) { return EMPTY; }
        return of(userInfoFaceBook);    }));
  }
  facebookLogiUsingFb() {
    // login with facebook and return observable with fb access token on success
    return from(new Promise<fb.StatusResponse>(resolve => FB.login(resolve, {
      scope: 'email',
      return_scopes: true
    } )))
      .pipe(concatMap(({ authResponse }) => {
        if (!authResponse) { return EMPTY; }
        return of(authResponse);
      }));
  }

  googleLogin(userData) {
    return  this.httpClient.post(this.APIurl + 'auth/google/login', userData ).pipe(map ((response: ResponseInterface) => {
        const userJson = response.data;
        this.commonService.setUserType(userJson.is_consultant);
        this.commonService.setJWTToken(userJson.token);
        return userJson;
      }),
      tap((mappedData) => {console.log(mappedData); }),
      catchError(err => {
        throw err;
      }));
  }
}
