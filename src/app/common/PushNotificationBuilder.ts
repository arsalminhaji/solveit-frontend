export class PushNotificationBuilder {
  public bodyMessage: string;
  public iconPath: string;
  public routePath: string;
  public notificationTitle: string;
  constructor(notificationBody, iconPath , routePath , notificationTitle) {
    this.bodyMessage = notificationBody;
    this.iconPath = iconPath;
    this.routePath = routePath;
    this.notificationTitle = notificationTitle;
  }

}
