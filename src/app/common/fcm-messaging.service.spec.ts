import { TestBed } from '@angular/core/testing';

import { FcmMessagingService } from './fcm-messaging.service';

describe('FcmMessagingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FcmMessagingService = TestBed.get(FcmMessagingService);
    expect(service).toBeTruthy();
  });
});
