import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {CommonService} from '../common.service';

@Injectable({
  providedIn: 'root'
})
export class JwtGuardGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router, private commonService: CommonService ) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivateChild(next, state);
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const currentUser = this.commonService.getCurrentUser();
    if (currentUser) {
      return true;
    }
    this.router.navigate(['/auth'], {});
    return false;
  }

}
