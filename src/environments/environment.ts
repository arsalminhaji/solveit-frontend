// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  agora: {
    appId: 'a7a735d737e744a1aed276da9e928621'
  },
  firebase : {
    apiKey: 'AIzaSyCEnqV1qmhZ3pi7KPrrfgCsEtkKZxONGLU',
    authDomain: 'solveit-d11ce.firebaseapp.com',
    databaseURL: 'https://solveit-d11ce.firebaseio.com',
    projectId: 'solveit-d11ce',
    storageBucket: 'solveit-d11ce.appspot.com',
    messagingSenderId: '641727759299',
    appId: '1:641727759299:web:713610cfe62c841ef57376'
  },
  facebookAppId: '147882410745294',
  googleProviderId: '190073574896-r53uvlr2fv26co57i0v7ksu3db9f0bu2.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
