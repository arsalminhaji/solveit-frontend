var isRTL = ($('html').attr('dir') == "rtl") ? true : false,
    winWidth = $(window).width(),
	winHeight = $(window).height(),
	listHeight = 0;

$(function() {
    browserDetect();
});

$(window).on('load', function() {
    // Do after Page ready
    doOnLoad();
	toggleLoginTab();
	initDropdown();
	initMobileDropdown();
	// toSvg();
	getVisibleHeight();
  initCartStickyTab();
  toggleFilter();
});

$(window).on('resize orientationchange', function() {
    // Do on resize
    winWidth = $(window).width(),
	winHeight = $(window).height();
  toggleFilter()
});

$(window).on('scroll', function() {
  //Do on Scroll
  initCartStickyTab();
});


$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        //Do on ESC press
    }
});

function doOnLoad() {
    //OnLoad Function
    setTimeout(function() {
        addVideoPlugin();
    }, 4000);
}

$('.navbar-toggle').on('click', function() {
	listHeight = 0;
	setTimeout(() => {
		getVisibleHeight();
	}, 100);
})

function browserDetect() {
    navigator.sayswho = (function() {
        var ua = navigator.userAgent,
            tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) return tem.slice(1).join('').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();
    $('body').addClass(navigator.sayswho);
}

function addVideoPlugin() {
    if (winWidth > 1024 && $('.js-video').get(0)) {
        var plyrScriptElement = document.createElement("script");
        if (isRTL) {
            plyrScriptElement.setAttribute('src', customVariables.baseURL + '../assets/js/plyr.min.js');
        } else {
            plyrScriptElement.setAttribute('src', customVariables.baseURL + 'assets/js/plyr.min.js');
        }
        plyrScriptElement.setAttribute('async', 'true');
        document.body.appendChild(plyrScriptElement);
    } else {
        jsVideoDirect();
    }
}

var players = [];

function jsVideo() {
    // Custom player
    if ($('.js-video').length) {
        $('.js-video').each(function(i) {
            var thisParent = $(this).parent();
            players[i] = new Plyr(this, {
                playsinline: true,
            });
            thisParent.find('.plyr').attr('data-video-instance', i);
        });
    }
}

function jsVideoDirect() {
    if ($('.js-video').length) {
        $('.js-video').each(function(i) {
            $(this).attr('data-video-instance', i);
            var videoId = $(this).attr('data-plyr-embed-id');
            $(this).html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + videoId + '?rel=0&playsinline=1&enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
        });
    }
}

function toggleLoginTab() {
	$('.js-toggle').on('click', function(e){
        let $this = $(e.currentTarget),
            data = $this.attr('data-target'),
            target = $(`.${data}`);

        $('.card').removeClass('show');
        $('.btn').removeClass('is--active');

        $this.addClass('is--active');
        $(target).addClass('show');
    })
}

function initDropdown() {
	$('.js-dropdown').on('click', function(e) {
		let $this = $(e.currentTarget),
			parent = $this.parents('.nav-dropdown'),
			targetData = $this.attr('data-dropdown'),
			target = parent.find(`.${targetData}`);

		target.hasClass('show') ? target.removeClass('show') : target.addClass('show');
	})
}

function initMobileDropdown() {
	$('.js-dropdownMobile').on('click', function(e) {
		let $this = $(e.currentTarget),
			parent = $this.parents('.nav-dropdown'),
			targetData = $this.attr('data-dropdown'),
			target = parent.find(`.${targetData}`),
			height = target.attr('data-height');

		target.hasClass('height--mobile') ? target.removeClass('height--mobile') : target.addClass('height--mobile');
		target.css('height', height);
	})
}

// function scrollToPos() {
// 	let list = $('.js-listing'),
// 		duration = 1000;

// 	list.find('a').click(function(e) {
// 		let $this = $(e.currentTarget),
// 			getData = $this.attr('data-id'),
// 			getDataParent = $(`#${getData}`).parents('.post-block'),
// 			headerHeight = $('header').outerHeight() + 20;

// 		$('html, body').animate({
// 			scrollTop: $(getDataParent).offset().top - headerHeight
// 		}, duration);
// 	});
// }

/* ========================================================================= */
/*	Start Nav Sticky with clickable content
/* ========================================================================= */

var topRange = 220, // measure from the top of the viewport to 'n' pixels down
  edgeMargin = 20, // margin above the top or margin from the end of the page
  animationTime = 500, // time in milliseconds
  oldTop = 0,
  headerContainer = null,
  btnGroupToggle = null,
  contentSelectors = [];

$(window).on('load', function() {
  headerContainer = $('header');
  btnGroupToggle = $('.page-listing ul a');
  if (btnGroupToggle.length) {
    onScrollnav();
    setTimeout(() => {
      scrollToLocation();
    }, animationTime);
  }
})

function scrollToLocation() {
  let target = window.location.hash,
    headerHeight = $('header').outerHeight() + 20,
    targetToMove = $(target).parents('.post-block');

  if (target !== ""  && $(target).length) {
    console.log('scroll');
    $('html, body').animate({
      scrollTop: $(targetToMove).offset().top - headerHeight
    }, animationTime);
  }
}

function onScrollnav() {
  // Set up content an array of locations
  btnGroupToggle.each(function (i, e) {
    contentSelectors.push($(this).attr('href'));
  })

  // Animate menu scroll to content
  btnGroupToggle.click(function () {
    // get content top or top position if at the document bottom
    winWidth < 991 ? $('.js-toggle-filter').click() : false;
    let $this = $(this).attr('href'),
      newTop = $(`${$this}`).length ? Math.min($($this).offset().top, $(document).height() - $(window).height()) : oldTop;
    oldTop = newTop;

    $('html, body').stop().animate({
      'scrollTop': newTop - btnGroupToggle.parent().height() - (headerContainer.outerHeight() * 2) // viewport move and its top spacing
    }, animationTime, 'swing');
    return false;
  })
  // adjust side menu
}

$(window).on('load scroll', function () {
  if (btnGroupToggle != null && btnGroupToggle.length) {
    let winTop = $(window).scrollTop(),
      bodyHt = $(document).height(),
      vpHt = $(window).height() - btnGroupToggle.parent().height() - headerContainer.outerHeight() + edgeMargin; // viewport height + margin

    $.each(contentSelectors, function (i, selector) {
      let loc = $(selector).length ? $(selector).offset().top : false;
      if ((loc > winTop + edgeMargin && (loc < winTop + topRange || (winTop + vpHt) >= bodyHt))) {
        $('.page-listing ul a').removeClass('active').eq(i).addClass('active');
      }
    })
  }
})

/* ========================================================================= */
/*	End Nav Sticky with clickable content
/* ========================================================================= */
function toggleFilter() {
  if(winWidth < 991) {
    let secHeight = $('section').outerHeight();
    headerHeight = $('header').outerHeight();
    $('.sticky-tab').length ? $('.sticky-tab').css({'height' : secHeight - 50 , 'top' : headerHeight + 50}) : false;
    $('.js-toggle-filter').on('click', function(){
      let target = $(this).parent().find('.sticky-tab');
      target.hasClass('is--active') ? target.removeClass('is--active') : target.addClass('is--active');
    })
  }
}

function initCartStickyTab() {
  if($('.js-sticky-tab').length) {
    let windowWidth = $(window).width(),
      scrollValue = $(window).scrollTop(),
      sectionHeight = $('.info-content').outerHeight(),
      sectionTop = $('.info-content').offset().top,
      headerHeight = $('header').outerHeight(),
      stickyHeight = $('.nav-sticky-wrap').outerHeight(),
      stickWidth = $('.js-sticky-tab').outerWidth(),
      stickyParentHeight = $('.js-sticky-tab').outerHeight(),
      stickyTop = $('.js-sticky-tab').offset().top,
      calcBotPos = stickyParentHeight - stickyHeight,
      calcBotScroll = scrollValue + stickyHeight + headerHeight;

    if (windowWidth > 991) {
      $('.nav-sticky-wrap').css('width', stickWidth);
      $('.nav-sticky-wrap').removeClass('is--sticky');
      $('.nav-sticky-wrap').css('transform', 'translateY(0px)');

      if (((scrollValue + headerHeight + 10) > stickyTop) && (calcBotScroll < (sectionHeight + sectionTop))) {
        $('.nav-sticky-wrap').addClass('is--sticky');
        $('.nav-sticky-wrap').css('transform', 'translateY(' + (headerHeight + 10) + 'px)');
      }

      if (calcBotScroll > (sectionHeight + sectionTop) - 10) {
        $('.nav-sticky-wrap').removeClass('is--sticky');
        $('.nav-sticky-wrap').css('transform', 'translateY(' + (calcBotPos) + 'px)');
      }

    } else {
      $('.nav-sticky-wrap').css('width', '100%');
      $('.nav-sticky-wrap').removeClass('is--sticky');
      $('.nav-sticky-wrap').addClass('sticky--bottom');
      $('.nav-sticky-wrap').css('transform', 'translateY(0px)');
    }
  }
}

function toSvg() {
    $('img.js-tosvg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function (data) {
            var $svg = $(data).find('svg');

            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }

            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' insvg');
            }

            $svg = $svg.removeAttr('xmlns:a');

            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
            }

            $img.replaceWith($svg);
        }, 'xml');
    });
}

function getVisibleHeight() {
	$('.js-getHeight > div').each(function (i, e) {
		let $this = $(e),
			contentHeight = $this.outerHeight();

		listHeight += contentHeight;
		$('.js-getHeight').attr('data-height', listHeight);
	});
}

(function ($) {
	'use strict';

	/* ========================================================================= */
	/*	Page Preloader
	/* ========================================================================= */

	$(window).on('load', function () {
		$('.preloader').fadeOut(700);
	});

	/* ========================================================================= */
	/*	Post image slider
	/* ========================================================================= */

	$('#post-thumb, #gallery-post').slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000

	});

	$('#features').slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000
	});


	/* ========================================================================= */
	/*	Menu item highlighting
	/* ========================================================================= */


	$('#navigation').sticky({
		topSpacing: 0
	});


	/* ========================================================================= */
	/*	Magnific popup
	/* =========================================================================  */
	$('.image-popup').magnificPopup({
		type: 'image',
		removalDelay: 160, //delay removal by X to allow out-animation
		callbacks: {
			beforeOpen: function () {
				// just a hack that adds mfp-anim class to markup
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		closeOnContentClick: true,
		midClick: true,
		fixedContentPos: false,
		fixedBgPos: true
	});



	//   magnific popup video
	$('.popup-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-zoom-in',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: true
	});
	/* ========================================================================= */
	/*	Portfolio Filtering Hook
	/* =========================================================================  */

	$(document).ready(function () {
		var containerEl = document.querySelector('.filtr-container');
		var filterizd;
		if (containerEl) {
			filterizd = $('.filtr-container').filterizr({});
		}
		//Active changer
		$('.portfolio-filter button').on('click', function () {
			$('.portfolio-filter button').removeClass('active');
			$(this).addClass('active');
		});
	});

	/* ========================================================================= */
	/*	Testimonial Carousel
	/* =========================================================================  */

	//Init the carousel
	$('#testimonials').slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000
	});

	/* ========================================================================= */
	/*   Contact Form Validating
	/* ========================================================================= */


	$('#contact-submit').click(function (e) {

		//stop the form from being submitted
		e.preventDefault();

		/* declare the variables, var error is the variable that we use on the end
		to determine if there was an error or not */
		var error = false;
		var name = $('#name').val();
		var email = $('#email').val();
		var subject = $('#subject').val();
		var message = $('#message').val();

		/* in the next section we do the checking by using VARIABLE.length
		where VARIABLE is the variable we are checking (like name, email),
		length is a JavaScript function to get the number of characters.
		And as you can see if the num of characters is 0 we set the error
		variable to true and show the name_error div with the fadeIn effect.
		if it's not 0 then we fadeOut the div( that's if the div is shown and
		the error is fixed it fadesOut.

		The only difference from these checks is the email checking, we have
		email.indexOf('@') which checks if there is @ in the email input field.
		This JavaScript function will return -1 if no occurrence have been found.*/
		if (name.length === 0) {
			var error = true;
			$('#name').css('border-color', '#D8000C');
		} else {
			$('#name').css('border-color', '#666');
		}
		if (email.length === 0 || email.indexOf('@') === '-1') {
			var error = true;
			$('#email').css('border-color', '#D8000C');
		} else {
			$('#email').css('border-color', '#666');
		}
		if (subject.length === 0) {
			var error = true;
			$('#subject').css('border-color', '#D8000C');
		} else {
			$('#subject').css('border-color', '#666');
		}
		if (message.length === 0) {
			var error = true;
			$('#message').css('border-color', '#D8000C');
		} else {
			$('#message').css('border-color', '#666');
		}

		//now when the validation is done we check if the error variable is false (no errors)
		if (error === false) {
			//disable the submit button to avoid spamming
			//and change the button text to Sending...
			$('#contact-submit').attr({
				'disabled': 'false',
				'value': 'Sending...'
			});

			/* using the jquery's post(ajax) function and a lifesaver
			function serialize() which gets all the data from the form
			we submit it to send_email.php */
			$.post('sendmail.php', $('#contact-form').serialize(), function (result) {
				//and after the ajax request ends we check the text returned
				if (result === 'sent') {
					//if the mail is sent remove the submit paragraph
					$('#cf-submit').remove();
					//and show the mail success div with fadeIn
					$('#mail-success').fadeIn(500);
				} else {
					//show the mail failed div
					$('#mail-fail').fadeIn(500);
					//re enable the submit button by removing attribute disabled and change the text back to Send The Message
					$('#contact-submit').removeAttr('disabled').attr('value', 'Send The Message');
				}
			});
		}
	});

})(jQuery);
// End Jquery Function

/* ========================================================================= */
/*	Animated section
/* ========================================================================= */

var wow = new WOW({
	offset: 100, // distance to the element when triggering the animation (default is 0)
	mobile: false // trigger animations on mobile devices (default is true)
});
wow.init();

/* ========================================================================= */
/*	Smooth Scroll
/* ========================================================================= */
var scroll = new SmoothScroll('a[href*=\'#\']');


/* ========================================================================= */
/*	Google Map Customization
/* =========================================================================  */

function initialize() {
	'use strict';

	var myLatLng = new google.maps.LatLng(22.333851, 91.812256);

	var roadAtlasStyles = [{
		'featureType': 'landscape',
		'elementType': 'geometry.fill',
		'stylers': [{
			'color': '#2F3238'
		}]
	}, {
		'elementType': 'labels.text.fill',
		'stylers': [{
			'color': '#FFFFFF'
		}]
	}, {
		'elementType': 'labels.text.stroke',
		'stylers': [{
			'visibility': 'off'
		}]
	}, {
		'featureType': 'road',
		'elementType': 'geometry.fill',
		'stylers': [{
			'color': '#50525f'
		}]
	}, {
		'featureType': 'road',
		'elementType': 'geometry.stroke',
		'stylers': [{
			'visibility': 'on'
		}, {
			'color': '#808080'
		}]
	}, {
		'featureType': 'poi',
		'elementType': 'labels',
		'stylers': [{
			'visibility': 'off'
		}]
	}, {
		'featureType': 'transit',
		'elementType': 'labels.icon',
		'stylers': [{
			'visibility': 'off'
		}]
	}, {
		'featureType': 'poi',
		'elementType': 'geometry',
		'stylers': [{
			'color': '#808080'
		}]
	}, {
		'featureType': 'water',
		'elementType': 'geometry.fill',
		'stylers': [{
			'color': '#3071a7'
		}, {
			'saturation': -65
		}]
	}, {
		'featureType': 'road',
		'elementType': 'labels.icon',
		'stylers': [{
			'visibility': 'off'
		}]
	}, {
		'featureType': 'landscape',
		'elementType': 'geometry.stroke',
		'stylers': [{
			'color': '#bbbbbb'
		}]
	}];

	var mapOptions = {
		zoom: 14,
		center: myLatLng,
		disableDefaultUI: true,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: false,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'roadatlas']
		}
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title: ''
	});


	google.maps.event.addListener(marker, 'click', function () {
		infowindow.open(map, marker);
	});

	var styledMapOptions = {
		name: 'US Road Atlas'
	};

	var usRoadMapType = new google.maps.StyledMapType(
		roadAtlasStyles, styledMapOptions);

	map.mapTypes.set('roadatlas', usRoadMapType);
	map.setMapTypeId('roadatlas');
}
