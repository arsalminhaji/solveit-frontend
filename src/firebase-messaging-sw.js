importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js');
firebase.initializeApp({
apiKey: "AIzaSyCEnqV1qmhZ3pi7KPrrfgCsEtkKZxONGLU",
authDomain: "solveit-d11ce.firebaseapp.com",
databaseURL: "https://solveit-d11ce.firebaseio.com",
projectId: "solveit-d11ce",
storageBucket: "solveit-d11ce.appspot.com",
messagingSenderId: "641727759299",
appId: "1:641727759299:web:713610cfe62c841ef57376",
});
self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  event.waitUntil(self.clients.openWindow(event.notification.data.url));
});
const messaging = firebase.messaging();
